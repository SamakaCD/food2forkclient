package com.teamvoy.food2fork;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.teamvoy.food2fork.rest.RestClient;
import com.teamvoy.food2fork.rest.models.Recipe;
import com.teamvoy.food2fork.rest.models.RecipesDetailedContainer;

public class DetailedRecipeFragment extends Fragment {
	
	Recipe mRecipe;

	public DetailedRecipeFragment(Recipe recipe) {
		mRecipe = recipe;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_recipe_detailed, container, false);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		ImageView image = (ImageView) getView().findViewById(R.id.f_recipe_detailed_image);
		Glide.with(getContext())
				.load(mRecipe.imageUrl)
				.centerCrop()
				.placeholder(R.drawable.recipes_grid_item_placeholder)
				.crossFade()
				.into(image);
		
		TextView title = (TextView) getView().findViewById(R.id.f_recipe_detailed_title);
		title.setText(mRecipe.title);
		
		TextView publisher = (TextView) getView().findViewById(R.id.f_recipe_detailed_publisher);
		publisher.setText(mRecipe.publisher);
		
		TextView publisherUrl = (TextView) getView().findViewById(R.id.f_recipe_detailed_publisher_url);
		publisherUrl.setText(mRecipe.publisherUrl);
		
		TextView rank = (TextView) getView().findViewById(R.id.f_recipe_detailed_rate);
		rank.setText(String.valueOf(Math.round(mRecipe.socialRank * 10) / 10));
		
		getView().findViewById(R.id.f_recipe_detailed_up).setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStackImmediate();
			}
		});
		
		loadIngredients();
	}
	
	void loadIngredients() {
		final TextView ingredientsTextView = (TextView) getView().findViewById(R.id.f_recipe_detailed_ingredients);
		
		Call<RecipesDetailedContainer> call = ((MainActivity) getActivity()).getRestClient()
				.getService().loadRecipe(RestClient.API_KEY, mRecipe.recipeId);

        call.enqueue(new Callback<RecipesDetailedContainer>() {
			
			@Override
			public void onResponse(Response<RecipesDetailedContainer> response, Retrofit retrofit) {
				ingredientsTextView.setVisibility(View.VISIBLE);
				ingredientsTextView.animate().setDuration(0).alpha(0).start();
				ingredientsTextView.animate().setDuration(150).alpha(1).start();
				
				StringBuilder stringBuilder = new StringBuilder();
				if(response.body().recipeDetailed == null)
					return;
				
				for (String str : response.body().recipeDetailed.ingredients)
					stringBuilder.append("�\t\t\t" + str.replace("\n", "") + ";\n");
				ingredientsTextView.setText(stringBuilder);
			}
			
			@Override
			public void onFailure(Throwable throwable) {
				
			}
			
		});
	}

}
