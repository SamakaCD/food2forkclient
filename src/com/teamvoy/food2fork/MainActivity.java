package com.teamvoy.food2fork;

import android.annotation.SuppressLint;
import android.app.ActivityManager.TaskDescription;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.teamvoy.food2fork.rest.RestClient;

public class MainActivity extends AppCompatActivity {
	
	RestClient mRestClient;
	RecipesFragment mRecipesFragment;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if(Build.VERSION.SDK_INT >= 21) {
			setTaskDescription(new TaskDescription(getResources().getString(R.string.app_name),
					BitmapFactory.decodeResource(getResources(), R.drawable.ic_recents),
					ContextCompat.getColor(this, R.color.color_primary)));
			
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
			getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.color_systemui_tint));
		}
		
		mRestClient = new RestClient();
		setupFragments();
	}
	
	public RestClient getRestClient() {
		return mRestClient;
	}
	
	void setupFragments() {
		mRecipesFragment = RecipesFragment.newInstance();
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.a_main_recipes, mRecipesFragment)
				.commit();
	}
	
	public RecipesFragment getRecipesFragment() {
		return mRecipesFragment;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mRecipesFragment.saveInstance();
		SavedInstanceSingleton.isRestoring = true;
	}
	
}
