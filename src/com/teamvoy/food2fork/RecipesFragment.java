package com.teamvoy.food2fork;

import java.util.LinkedHashMap;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionInflater;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.teamvoy.food2fork.rest.RestClient;
import com.teamvoy.food2fork.rest.models.RecipesContainer;
import com.teamvoy.food2fork.ui.GridViewWithHeaderAndFooter;
import com.teamvoy.food2fork.ui.RecipesGridViewAdapter;
import com.teamvoy.food2fork.ui.SimpleAnimatorListener;

public class RecipesFragment extends Fragment implements View.OnClickListener {
	
	Toolbar					    mToolbar;
	View					    mToolbarLayout;
	View						mSearchLayout;
	EditText					mSearchEdit;
	GridViewWithHeaderAndFooter mGridView;
	View					    mNoInternetView;
	TextView				    mErrorTextView;
	ProgressBar				    mLoadingProgressBar;
	View						mGridHeader;
	ProgressBar					mGridFooter;
	RecipesGridViewAdapter	    mGridViewAdapter;
	int 					    mLoadedPagesCount = 0;
	
	public static RecipesFragment newInstance() {
		return new RecipesFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_recipes_list, container, false);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		setupViews();
		loadRecipes();
	}
	
	public void saveInstance() {
		for(int i = 0; i < mGridViewAdapter.getCount(); i++)
			if(!SavedInstanceSingleton.SavedRecipes.wasRecipeSaved(mGridViewAdapter.getItem(i)))
				SavedInstanceSingleton.SavedRecipes.saveRecipe(mGridViewAdapter.getItem(i));
		
		mGridView.removeHeaderView(mGridHeader);
		mGridView.removeFooterView(mGridFooter);
	}
	
	public void reloadRecipes() {
		mGridViewAdapter.clear();
		SavedInstanceSingleton.SavedRecipes.clear();
		mLoadedPagesCount = 0;
		loadRecipes();
	}
	
	@SuppressLint("ClickableViewAccessibility")
	void setupViews() {
		mToolbar = (Toolbar) getView().findViewById(R.id.f_main_toolbar);
		mToolbarLayout = getView().findViewById(R.id.f_recipes_toolbar_container);
		setupToolbarMenu();
		
		mErrorTextView = (TextView) getView().findViewById(R.id.f_recipes_no_internet_text);
		mNoInternetView = getView().findViewById(R.id.f_recipes_no_internet_layout);
		mNoInternetView.findViewById(R.id.f_recipes_no_internet_retry_button).setOnClickListener(this);
		
		mLoadingProgressBar = (ProgressBar) getView().findViewById(R.id.f_recipes_progressbar);
		mGridView = (GridViewWithHeaderAndFooter) getView().findViewById(R.id.f_recipes_gridview);
		mGridView.setOnItemClickListener(mOnSelectRecipeListener);
		mGridView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) { }
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if(view.getAdapter() != null && (firstVisibleItem + visibleItemCount) >= totalItemCount)
					loadRecipes();	
			}
		});
		
		mGridView.setOnTouchListener(mToolbarScrollEventsListener);
		
		/* Wait until GridView size will measured */
		mGridView.post(new Runnable() {
			@Override
			public void run() {
				mGridHeader = new View(getActivity());
				mGridView.addHeaderView(mGridHeader);
				mGridHeader.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
				mGridHeader.getLayoutParams().height = (int) getResources().getDimension(R.dimen.appbar_height);
				mGridHeader.requestLayout();
				
				mGridFooter = new ProgressBar(getActivity());
				mGridFooter.setIndeterminate(true);
				mGridView.addFooterView(mGridFooter);
				mGridFooter.setVisibility(View.INVISIBLE);
				
				mGridViewAdapter = new RecipesGridViewAdapter(getActivity(), mGridView);
				mGridView.setAdapter(mGridViewAdapter);
				
				if(SavedInstanceSingleton.isRestoring) {
					for(int i = 0; i < SavedInstanceSingleton.SavedRecipes.getCount(); i++)
						mGridViewAdapter.add(SavedInstanceSingleton.SavedRecipes.getRecipe(i));
				}
			}
		});
		
		mSearchLayout = getView().findViewById(R.id.f_recipes_search_container);
		mSearchEdit = (EditText) getView().findViewById(R.id.f_recipes_search_edit);
		mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {				
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				mToolbar.setVisibility(View.VISIBLE);
				mSearchLayout.setVisibility(View.INVISIBLE);
			
				InputMethodManager inputManager = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				if(inputManager != null)
					inputManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
				
				RecipesFragment.this.find(v.getText().toString());
				return true;
			}
		});
		getView().findViewById(R.id.f_recipes_search_close).setOnClickListener(this);
	}
	
	void setupToolbarMenu() {
		mToolbar.getMenu().clear();
		new MenuInflater(getActivity()).inflate(R.menu.menu, mToolbar.getMenu());
		mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch(item.getItemId()) {
					case R.id.menu_search:
						mToolbar.setVisibility(View.INVISIBLE);
						mSearchLayout.setVisibility(View.VISIBLE);
						mSearchEdit.requestFocus();
						Toast.makeText(getActivity(), R.string.search_toast, Toast.LENGTH_SHORT).show();
						
						InputMethodManager inputManager = (InputMethodManager) getActivity()
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						if(inputManager != null)
							inputManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
						break;
					case R.id.menu_order:
						Utils.showOrderDialog(getActivity());
						break;
				}
				return false;
			}
		});
	}
	
	void find(String query) {
		setNoInternetViewVisibility(!Utils.hasInternetConnection(getActivity()), 
				getResources().getString(R.string.no_internet));
		mLoadingProgressBar.setVisibility(View.VISIBLE);
		mLoadedPagesCount = 0;
		
		mGridViewAdapter.clear();
		
		LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
		parameters.put("q", query);
		parameters.put("page", String.valueOf(mLoadedPagesCount + 1));
		
		Call<RecipesContainer> call = ((MainActivity) getActivity()).getRestClient()
				.getService()
				.loadRecipes(RestClient.API_KEY, parameters);
		call.enqueue(new Callback<RecipesContainer>() {
					
			@Override
			public void onResponse(final Response<RecipesContainer> response, Retrofit retrofit) {
				if(response.body().getRecipes() == null)
					return;
				
				hideProgressBar();
				getView().postDelayed(new Runnable() {		
					@Override
					public void run() {
						if(response.body().getRecipes().size() == 0) {
							Snackbar.make(getView(), R.string.no_matches_found, Snackbar.LENGTH_LONG).show();
						} else {
							mLoadedPagesCount++;
							mGridViewAdapter.addAll(response.body().getRecipes());
							mGridFooter.setVisibility(View.VISIBLE);
						}
					}
				}, 300);
			}
			
			@Override
			public void onFailure(Throwable throwable) {
				String errorText = getResources().getString(R.string.error_while_recipes_request);
				Utils.log(errorText);
				throwable.printStackTrace();
				hideProgressBar();
				getView().postDelayed(new Runnable() {
					@Override
					public void run() {
						setNoInternetViewVisibility(true, getResources().getString(R.string.error_while_recipes_request));
					}
				}, 300);
			}
					
		});
	}
	
	void loadRecipes() {
		setNoInternetViewVisibility(!Utils.hasInternetConnection(getActivity()), 
				getResources().getString(R.string.no_internet));
		mLoadingProgressBar.setVisibility(View.VISIBLE);
		
		LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
		parameters.put("sort", Utils.getOrder(getActivity()));
		parameters.put("page", String.valueOf(mLoadedPagesCount + 1));
		
		Call<RecipesContainer> call = ((MainActivity) getActivity()).getRestClient()
				.getService()
				.loadRecipes(RestClient.API_KEY, parameters);
		call.enqueue(new Callback<RecipesContainer>() {
					
			@Override
			public void onResponse(final Response<RecipesContainer> response, Retrofit retrofit) {
				if(response.body().getRecipes() == null) {
					setNoInternetViewVisibility(true, getResources().getString(R.string.error_while_recipes_request));
					return;
				}
				
				hideProgressBar();
				getView().postDelayed(new Runnable() {		
					@Override
					public void run() {
						mLoadedPagesCount++;
						mGridViewAdapter.addAll(response.body().getRecipes());
						mGridFooter.setVisibility(View.VISIBLE);
					}
				}, 300);
			}
			
			@Override
			public void onFailure(Throwable throwable) {
				String errorText = getResources().getString(R.string.error_while_recipes_request);
				Utils.log(errorText);
				throwable.printStackTrace();
				hideProgressBar();
				getView().postDelayed(new Runnable() {
					@Override
					public void run() {
						setNoInternetViewVisibility(true, getResources().getString(R.string.error_while_recipes_request));
					}
				}, 300);
			}
					
		});
	}
	
	void hideProgressBar() {
		mLoadingProgressBar.setPivotX(mLoadingProgressBar.getWidth() / 2);
		mLoadingProgressBar.setPivotY(mLoadingProgressBar.getHeight() / 2);
		mLoadingProgressBar.animate()
				.setDuration(300)
				.setInterpolator(new AccelerateInterpolator())
				.setListener(new SimpleAnimatorListener() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mLoadingProgressBar.setVisibility(View.GONE);
					}
				})
				.alpha(0)
				.scaleX(0.8f)
				.scaleY(0.8f)
				.start();
	}
	
	void setNoInternetViewVisibility(boolean visible, String errorText) {
		if(visible) {
			hideProgressBar();
			mNoInternetView.setVisibility(View.VISIBLE);
			mNoInternetView.animate()
					.setDuration(0)
					.alpha(0)
					.start();
			mNoInternetView.animate()
					.setDuration(150)
					.alpha(1)
					.start();
			mErrorTextView.setText(errorText);
			return;
		} else if(mNoInternetView.getVisibility() == View.VISIBLE) {
			mNoInternetView.animate()
					.setDuration(150)
					.setListener(new SimpleAnimatorListener() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mNoInternetView.setVisibility(View.GONE);
						}
					})
					.alpha(0)
					.start();
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.f_recipes_no_internet_retry_button:
				loadRecipes();
				break;
			case R.id.f_recipes_search_close:
				mToolbar.setVisibility(View.VISIBLE);
				mSearchLayout.setVisibility(View.INVISIBLE);

				InputMethodManager inputManager = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				if(inputManager != null)
					inputManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
				break;
		}
	}
	
	AdapterView.OnItemClickListener mOnSelectRecipeListener = new AdapterView.OnItemClickListener() {
		@SuppressLint({ "InlinedApi", "NewApi" })
		@Override
		public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setSharedElementReturnTransition(TransitionInflater.from(getActivity())
                		.inflateTransition(R.transition.change_image_transform));
                setExitTransition(TransitionInflater.from(getActivity())
                		.inflateTransition(android.R.transition.explode));

                Fragment fragment = new DetailedRecipeFragment(mGridViewAdapter.getItem(position));
                fragment.setSharedElementEnterTransition(TransitionInflater.from(getActivity())
                		.inflateTransition(R.transition.change_image_transform));
                fragment.setEnterTransition(TransitionInflater.from(getActivity())
                		.inflateTransition(android.R.transition.explode));

                getFragmentManager().beginTransaction()
                        .replace(R.id.a_main_recipe_detailed, fragment)
                        .addToBackStack("transaction")
                        .addSharedElement(view.findViewById(R.id.i_recipes_grid_image), "open_details")
                		.commit();
            } else {
            	Fragment fragment = new DetailedRecipeFragment(mGridViewAdapter.getItem(position));
                getFragmentManager().beginTransaction()
                        .replace(R.id.a_main_recipe_detailed, fragment)
                        .addToBackStack("transaction")
                		.commit();
            }
		}
	};
	
	View.OnTouchListener mToolbarScrollEventsListener = new View.OnTouchListener() {
		
		float mFirstTouchY;
		boolean mCalculatingAllowed;
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch(event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					v.performClick();
					mFirstTouchY = event.getY();
					mCalculatingAllowed = true;
					break;
				case MotionEvent.ACTION_MOVE:
					if(mGridViewAdapter.getCount() < 4 || !mCalculatingAllowed)
						return false;
					
					if((event.getY() - mFirstTouchY) > Utils.dpToPx(getActivity(), 56)) {
						mCalculatingAllowed = false;
						mToolbarLayout.animate()
								.setDuration(300)
								.setInterpolator(new DecelerateInterpolator(2))
								.translationY(0)
								.start();
					} else if((event.getY() - mFirstTouchY) < Utils.dpToPx(getActivity(), -56)) {
						mCalculatingAllowed = false;
						mToolbarLayout.animate()
								.setDuration(300)
								.setInterpolator(new AccelerateInterpolator(2))
								.translationY(-mToolbarLayout.getHeight())
								.start();
					}
					
					break;
			}
			return false;
		}
	};
	
}
