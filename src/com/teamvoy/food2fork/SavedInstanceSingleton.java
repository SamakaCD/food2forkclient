package com.teamvoy.food2fork;

import java.util.ArrayList;

import com.teamvoy.food2fork.rest.models.Recipe;

public class SavedInstanceSingleton {
	
	public static boolean isRestoring;

	/* I made this card implementation, because it was 
	 * necessary to save order data and access data by index */
	public static class SavedRecipes {
		
		static ArrayList<String> keys = new ArrayList<>();
		static ArrayList<Recipe> values = new ArrayList<>();
		
		public static void saveRecipe(Recipe recipe) {
			keys.add(recipe.recipeId);
			values.add(recipe);
		}
		
		public static Recipe getRecipe(int index) {
			return values.get(index);
		}
		
		public static void clear() {
			keys.clear();
			values.clear();
		}
		
		public static boolean wasRecipeSaved(Recipe recipe) {
			return keys.contains(recipe.recipeId);
		}
		
		public static int getCount() {
			return keys.size();
		}
		
	}
	
}