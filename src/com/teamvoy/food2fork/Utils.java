package com.teamvoy.food2fork;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Utils {

	public static void log(Object... args) {
		StringBuilder stringBuilder = new StringBuilder();
		for(Object object : args)
			stringBuilder.append(object).append(" ");
		Log.d("Food2Fork", stringBuilder.toString());
	}
	
	public static int dpToPx(Context context, float dp) {
		return (int) (context.getResources().getDisplayMetrics().density * dp);
	}
	
	public static boolean hasInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || 
            		activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return true;
        return false;
    }
	
	static final String[] ORDER_VALUES = new String[] {
		"r", "t"
	};
	
	public static void showOrderDialog(final Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
				.setTitle(R.string.set_results_order)
				.setItems(R.array.sort_order_variants, new DialogInterface.OnClickListener() {		
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreferences.Editor sharedPreferences = 
								context.getSharedPreferences("settings", Context.MODE_PRIVATE)
								.edit();
						sharedPreferences.putString("order", ORDER_VALUES[which]);
						sharedPreferences.commit();
						
						((MainActivity) context).getRecipesFragment().reloadRecipes();
					}
				});
		builder.show();
	}
	
	public static String getOrder(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
		return sharedPreferences.getString("order", "r");
	}
	
}