package com.teamvoy.food2fork.rest;

import java.util.Map;

import com.teamvoy.food2fork.rest.models.RecipesContainer;
import com.teamvoy.food2fork.rest.models.RecipesDetailedContainer;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;

public interface F2FService {

	@GET("search")
	Call<RecipesContainer> loadRecipes(@Query("key") String apiKey, @QueryMap Map<String, String> parameters);
	
	@GET("get")
    Call<RecipesDetailedContainer> loadRecipe(@Query("key") String apiKey, @Query("rId") String id);

}