package com.teamvoy.food2fork.rest;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RestClient {

	public static final String API_KEY = "3104f6f7e4472f02c00ae156ce8dc4ba";
	public static final String API_URL = "http://food2fork.com/api/";

	F2FService mF2FApiService;

	public RestClient() {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(API_URL)
				.addConverterFactory(GsonConverterFactory.create()).build();
		mF2FApiService = retrofit.create(F2FService.class);
	}

	public F2FService getService() {
		return mF2FApiService;
	}

}
