package com.teamvoy.food2fork.rest.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Recipe implements Parcelable {

	public String title;
	public String publisher;

	@SerializedName("f2f_url")
	public String f2fUrl;

	@SerializedName("source_url")
	public String sourceUrl;

	@SerializedName("recipe_id")
	public String recipeId;

	@SerializedName("image_url")
	public String imageUrl;

	@SerializedName("social_rank")
	public Double socialRank;

	@SerializedName("publisher_url")
	public String publisherUrl;

	Recipe(Parcel in) {
		title        = in.readString();
		publisher    = in.readString();
		f2fUrl       = in.readString();
		sourceUrl    = in.readString();
		recipeId     = in.readString();
		imageUrl     = in.readString();
		socialRank   = in.readDouble();
		publisherUrl = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(publisher);
		dest.writeString(f2fUrl);
		dest.writeString(sourceUrl);
		dest.writeString(recipeId);
		dest.writeString(imageUrl);
		dest.writeDouble(socialRank);
		dest.writeString(publisher);
	}

	public static final Parcelable.Creator<Recipe> CREATOR = new Parcelable.Creator<Recipe>() {
		@Override
		public Recipe createFromParcel(Parcel in) {
			return new Recipe(in);
		}

		@Override
		public Recipe[] newArray(int size) {
			return new Recipe[size];
		}
	};

}
