package com.teamvoy.food2fork.rest.models;

import java.util.List;

import android.os.Parcel;

public class RecipeDetailed extends Recipe {
	
	public List<String> ingredients;

	RecipeDetailed(Parcel in) {
		super(in);
	}

}