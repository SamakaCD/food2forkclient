package com.teamvoy.food2fork.rest.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RecipesContainer {

	@SerializedName("count")
	Integer 		mCount;
	
	@SerializedName("recipes")
	List<Recipe>	mRecipes;

	public int getCount() {
		return mCount;
	}

	public List<Recipe> getRecipes() {
		return mRecipes;
	}

}
