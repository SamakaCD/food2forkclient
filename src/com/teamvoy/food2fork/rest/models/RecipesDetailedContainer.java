package com.teamvoy.food2fork.rest.models;

import com.google.gson.annotations.SerializedName;

public class RecipesDetailedContainer {

	@SerializedName("recipe")
	public RecipeDetailed recipeDetailed;
	
}
