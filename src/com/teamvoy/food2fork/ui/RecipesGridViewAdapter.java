package com.teamvoy.food2fork.ui;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.teamvoy.food2fork.R;
import com.teamvoy.food2fork.Utils;
import com.teamvoy.food2fork.rest.models.Recipe;

public class RecipesGridViewAdapter extends ArrayAdapter<Recipe> {
	
	static class ViewHolder {
		
		public ImageView imageView;
		public TextView  titleView;
		
		public ViewHolder(ImageView imageView, TextView title) {
			this.imageView = imageView;
			this.titleView = title;
		}
		
	}
	
	static ArrayMap<String, Integer> mPaletteCache = new ArrayMap<>();
	
	LayoutInflater mLayoutInflater;
	int	mItemWidth;

	public RecipesGridViewAdapter(Context context, GridView gridView) {
		super(context, 0);
		mLayoutInflater = LayoutInflater.from(context);
		
		int columnsCount = (int) Math.floor(gridView.getWidth() / Utils.dpToPx(context, 160));
		gridView.setNumColumns(columnsCount);
		mItemWidth = (gridView.getWidth() - Utils.dpToPx(getContext(), 4) * (columnsCount - 1)) / columnsCount;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.item_recipes_grid, parent, false);
			convertView.getLayoutParams().width = mItemWidth;
			convertView.getLayoutParams().height = mItemWidth + Utils.dpToPx(getContext(), 68);
			holder = new ViewHolder(
					(ImageView) convertView.findViewById(R.id.i_recipes_grid_image), 
					(TextView) convertView.findViewById(R.id.i_recipes_grid_title));
			
			holder.imageView.getLayoutParams().width 
					= holder.imageView.getLayoutParams().height = mItemWidth;
			
			convertView.setTag(holder);
		} else holder = (ViewHolder) convertView.getTag();
		
		final Recipe recipe = getItem(position);
		holder.titleView.setText(recipe.title);
		
		DrawableTypeRequest<String> glideRequest = Glide.with(getContext()).load(recipe.imageUrl);
		
		if(mPaletteCache.containsKey(recipe.imageUrl)) {
			holder.titleView.setBackgroundColor(mPaletteCache.get(recipe.imageUrl));
		} else {
			glideRequest.listener(GlidePalette.with(recipe.imageUrl)
					.use(GlidePalette.Profile.VIBRANT)
					.intoCallBack(new BitmapPalette.CallBack() {
						@Override
						public void onPaletteLoaded(Palette palette) {
							mPaletteCache.put(recipe.imageUrl, palette.getVibrantColor(
									ContextCompat.getColor(getContext(), R.color.color_primary_dark)));
						}
					})
					.intoBackground(holder.titleView));
		}			
		
		glideRequest.centerCrop()
				.placeholder(R.drawable.recipes_grid_item_placeholder)
				.crossFade()
				.into(holder.imageView);
		
		return convertView;
	}
	
}
